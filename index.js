var head = document.getElementsByTagName("head")[0];
script = document.createElement('script');



script.onload = function() {

		// manage unexpected conflicts
    var $ = jQuery.noConflict();
    // jQuery is available now
    function initializeIt(){

    // start Initializing elements
      $('.overlay').css( {
              'position':'fixed',
              'top':0,
              'left':0,
              'width':'100%',
              'height':'100%',
              'background':'#000',
              'opacity':0.5,
              'filter':'alpha(opacity=50)'
            });

    $('.modal').css( {
            'position':'relative',
            'background':'url(tint20.png) 0 0 repeat',
            'background':'rgba(0,0,0,0.2)',
            'border-radius':'14px',
            'padding':'8px'
          });

    /* custom style*/

    $('.my-message-block').css( {
            'position': 'absolute',
            'min-width': '300px',
            'display': 'inline-block',
            'bottom': 50,
            'right': 50
          });
    $('.my-message-head').css( {
            'background-color': '#F2F2F2',
            'padding': '10px 15px',
            'width': '300px',
            'position': 'relative',
            'display': 'block',
            'border-top-left-radius': '6px',
            'border-top-right-radius': '6px',
            'border-bottom': '1px solid #ddd',
            'line-height': 0
          });
    $('.my-message-head h2').css( {
            'margin': 0,
            'line-height': 1.4,
            'font-family': 'Arial',
            'color': '#656668',
            'font-weight': 'bold',
            'font-size': '14px'
          });
    $('.my-message-head p').css( {
            'margin': 0,
            'line-height': 1.4,
            'font-family': 'Arial',
            'color': '#ADADAD',
            'font-size': '10px'
          });
    $('.avatar').css ({
              'position': 'relative',
              '-webkit-border-radius': '50%',
              '-moz-border-radius': '50%',
              'border-radius': '50%',
              'width': '45px',
              'height': '45px',
              'overflow': 'hidden',
              'display': 'inline-block',
              'margin-right': '15px'
          });
    $('.close-btn').css( {
            'float': 'right'
          });
    $('.close-btn img').css ({
            'width': '24px',
            'height': '24px',
            'opacity': 0.5
          });
    $('.avatar img').css( {
            'width': '100%',
            'height': 'auto'
          });
    $('.info').css( {
            'display': 'inline-block',
            'position': 'absolute',
            'max-width': '60%',
            'top': '50%',
        'transform': 'translateY(-50%)'
          });
    $('.close-btn-cont').css( {
            'display': 'inline-block',
            'float': 'right',
            'margin': '11px 0'
          });
    $('.my-message-footer').css ({
            'background-color': '#fff',
            'padding': '15px',
            'position': 'relative',
            'display': 'block',
            'width': '300px',
            'border-bottom-left-radius': '6px',
            'border-bottom-right-radius': '6px'
          });
    $('.my-message-footer p').css( {
            'margin': 0,
            'line-height': 1.4,
            'font-family': 'Arial',
            'color': '#656668',
            'font-weight': 200,
            'font-size': '16px'
          });
    $('.plus-icon-cont').css( {
            'position': 'absolute',
            'right': '-20px',
            'bottom': '-20px',
            'line-height': 0,
            'background-color': '#fff',
            'border-radius': '50%'
          });
    $('.plus-icon-cont img').css( {
            'width': '50px',
            'height': '50px',
          });
  // end
  }

var modal = (function(){
      var
      method = {},
      $overlay,
      $modal;

      // Center the modal in the viewport
      method.center = function () {
        var bottom, right;


        $modal.css({
          bottom:bottom,
          right:right
        });
      };

      // Open the modal
      method.open = function () {

        $(window).bind('resize.modal');
        $modal.show();
        $overlay.show();
      };


      // Close the modal
      method.close = function () {
        $modal.hide();
        $overlay.hide();
        //$content.empty();
        $(window).unbind('resize.modal');
      };

      // Generate the HTML and add it to the document
      $howdy = $('<a id=\'howdy\' class="howdy" href=\'#\' style="text-decoration: none;position: fixed;bottom: 0px;right: 0px; "><img src="http://i66.tinypic.com/282nyvl.png" alt="avatar" /></a>');
      $overlay = $('<div id="overlay" class="overlay"></div>');
      $modal = $('<div id="modal" class="my-message-block modal"><div class="my-message-head"><div class="avatar"><img src="http://autokadabra.ru/system/uploads/users/20/20303/small.png?1319912650" alt="avatar" /></div><div class="info"><h2>Denys Schotkin</h2><p>Founder & CTO</p></div><div class="close-btn-cont" style="display: inline;"><a href="#" id="close" class="close-btn closeit" onclick=""><img src="http://i66.tinypic.com/33udekk.png" alt=""></a></div></div><div class="content my-message-footer"><p>Do yo need help ?</p><div class="plus-icon-cont"><a class="closeit" href="#"><img src="http://i67.tinypic.com/2w4y8ut.png" width="100" height="100" alt="" onclick=""></a></div></div></div>');

      $modal.hide();
      $overlay.hide();
      //$modal.append($close);

      $(document).ready(function(){
        $('body').append($overlay, $modal, $howdy);


      });


      return method;
    }());



    // Wait until the DOM has loaded before querying the document
    $(document).ready(function(){

      $('a#howdy').click(function(e){
        $(this).hide();
        initializeIt();
        getjsoncontent();
        e.preventDefault();
      });

      $('.closeit').click(function(e){
        var elt1 = $('.modal');
        var elt2 = $('.overlay');
        var elt3 = $('.howdy');
        console.log('mes tests...');
        elt1.hide();
        elt2.hide();
        elt3.show();
      });

      /*function closeit(elt1, elt2, elt3){

              elt1.hide();
              elt2.hide();
              elt3.show();
      }*/

    });

    function getjsoncontent(){
      $.getJSON( "https://dev.nexusmedia-ua.com/devtest/", function( data ) {
        $('.info h2').html(data.messages[0].name);
        $('.info p').html(data.messages[0].title);
        $('.content p').html(data.messages[0].message);
        $(".avatar img").attr("src", data.messages[0].image);
        modal.open();
        console.log(data.messages[0].name);
      });
    }


};

script.type = 'text/javascript';
script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';

head.appendChild(script);
